================
Getting Started
================


Tutorial
========

First, make sure that the configuration files are placed within the current directory and the settings are the desired ones. To generate those files, run

.. code-block::

    pyhard init


This will create both ``config.yaml`` and ``options.json`` in the current directory.

The file ``config.yaml`` is used to configurate steps 1-4 below. Through it, options for file paths, measures, classifiers, feature selection and hyper-parameter optimization can be set. More instructions can be found in the comments within the file.

At least the field ``datafile`` (in section 'general') should be set in ``config.yaml``. It specifies the path (absolute or relative) of the input dataset. Leaving the field ``rootdir`` as ``'.'`` (default), the output files will be saved in the current folder along with the configuration files (recommended).

Once everything is configured, run the analysis:

.. code-block::

    pyhard run


By default, the following steps shall be taken:

1. Calculate the *hardness measures*;

2. Evaluate classification performance at instance level for each algorithm;

3. Select the most relevant hardness measures with respect to the instance classification error;

4. Join the outputs of steps 1, 2 and 3 to build the *metadata* file (``metadata.csv``);

5. Run **ISA** (*Instance Space Analysis*), which generates the *Instance Space* (IS) representation and the *footprint* areas;

Steps 1 to 4 comprise the metadata construction, and step 5 the ISA itself. To curb any of these two major stages, use the options with command ``run``:

* ``--no-meta``: does not attempt to build the metadata file

* ``--no-isa``: prevents the Instance Space Analysis

Finally, to explore the results, launch the app:

.. code-block::

    pyhard app


To see all CLI commands, run ``pyhard --help``, or ``pyhard run --help`` to get the options for this command.


Guidelines for input dataset
============================

Please follow the recommendations below:

* Only ``csv`` files are accepted

* The dataset should be in the format ``(n_instances, n_features)``

* It cannot contains NaNs or missing values

* **Do not** include any index column. Instances will be indexed in order, starting from **1**

* **The last column** should contain the target variable (``y``). Otherwise, the name of the target column must be declared in the field ``target_col`` (``config.yaml``)

* Categorical features should be handled previously

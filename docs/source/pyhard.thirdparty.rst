=========================
Thirdparty packages
=========================


Entropy Estimators
=========================

.. automodule:: pyhard.thirdparty.entropy_estimators
   :members:
   :undoc-members:
   :show-inheritance:

Rank Aggregation
=========================

.. automodule:: pyhard.thirdparty.rank_aggregation
   :members:
   :undoc-members:
   :show-inheritance:

skfeature
=========================

.. automodule:: pyhard.thirdparty.skfeature
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
=========================

.. automodule:: pyhard.thirdparty
   :members:
   :undoc-members:
   :show-inheritance:

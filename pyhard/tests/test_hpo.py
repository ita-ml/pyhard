import pytest

from pyhard.hpo import find_best_params, set_hyperopt_progressbar
from pyhard.classification import _clf_dict
from pyhard.regression import _reg_dict


@pytest.mark.parametrize("alias, predictor", _clf_dict.items())
def test_hpo_classification(alias, predictor, classification_dataset):
    set_hyperopt_progressbar(False)

    X = classification_dataset.iloc[:100, :-1]
    y = classification_dataset.iloc[:100, -1]

    best_params = find_best_params(
        alias=alias,
        predictor=predictor,
        X=X,
        y=y,
        max_evals=10
    )

    assert isinstance(best_params, dict)


@pytest.mark.parametrize("alias, predictor", _reg_dict.items())
def test_hpo_regression(alias, predictor, regression_dataset):
    set_hyperopt_progressbar(False)

    X = regression_dataset.iloc[:100, :-1]
    y = regression_dataset.iloc[:100, -1]

    best_params = find_best_params(
        alias=alias,
        predictor=predictor,
        X=X,
        y=y,
        max_evals=10
    )

    assert isinstance(best_params, dict)

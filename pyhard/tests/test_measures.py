from pathlib import Path

import numpy as np
import pandas as pd
import pytest

from pyhard.measures import ClassificationMeasures, RegressionMeasures


def toy_data_generator():
    data_dir = Path("../data")
    for path in data_dir.rglob("data.csv"):
        data = pd.read_csv(path)
        data.index.name = 'Instances'
        yield path.parent.name, ClassificationMeasures(data)


@pytest.mark.parametrize("measure", ClassificationMeasures._measures_dict.values())
@pytest.mark.parametrize("dataset, obj", toy_data_generator())
def test_classification_measures(measure, dataset, obj):
    result = obj._call_method(measure)
    assert isinstance(result, np.ndarray)
    assert ((result >= 0).all() and (result <= 1).all())


@pytest.mark.parametrize("measure", RegressionMeasures._measures_dict.values())
def test_regression_measures(measure, regression_dataset):
    result = RegressionMeasures(regression_dataset)._call_method(measure)
    assert isinstance(result, np.ndarray)
    assert ((result >= 0).all() and (result <= 1).all())

import pandas as pd
from pathlib import Path
from typer.testing import CliRunner

from pyhard.cli import cli
from pyhard.structures import Configurations, from_dict
from pyhard.utils import load_yaml_file, write_yaml_file


runner = CliRunner()


def clean_folder(path: Path):
    [f.unlink() for f in path.glob("*") if f.is_file()]


def test_no_config_file():
    result = runner.invoke(cli, ['run'])
    assert result.exit_code == 1
    assert "File 'config.yaml' not found." in result.stdout

    result = runner.invoke(cli, ['run', '--config', 'config_fake.yaml'])
    assert result.exit_code == 2
    assert "'config_fake.yaml' does not exist" in result.stdout


def test_config(rootdir_path, classification_dataset):
    conf = from_dict(load_yaml_file(rootdir_path / "config.yaml"))
    assert isinstance(conf, Configurations)


def test_no_metadata(rootdir_path, classification_dataset):
    classification_dataset.to_csv(rootdir_path / "data.csv", index=False)

    result = runner.invoke(cli, ['run', '--config', str(rootdir_path / "config.yaml"), '--no-meta'])
    assert result.exit_code == 1
    assert f"File '{rootdir_path / 'metadata.csv'}' missing." in result.stdout

    df_meta = pd.DataFrame(columns=['instances'])
    df_meta.to_csv(rootdir_path / 'metadata.csv')

    result = runner.invoke(cli, ['run', '--config', str(rootdir_path / "config.yaml"), '--no-meta'])
    assert result.exit_code == 1
    assert f"File '{rootdir_path / 'ih.csv'}' missing." in result.stdout


def test_separate_steps(rootdir_path, classification_dataset):
    classification_dataset.to_csv(rootdir_path / "data.csv", index=False)

    result = runner.invoke(cli, ['run', '--config', str(rootdir_path / "config.yaml"), '--no-isa'])

    assert result.exit_code == 0
    expected_files = [
        'metadata.csv',
        'ih.csv'
    ]
    for file in expected_files:
        assert (rootdir_path / file).exists()
    assert "Building metadata." in result.stdout
    assert "Instance Hardness analysis finished." in result.stdout

    result = runner.invoke(cli, ['run', '--config', str(rootdir_path / "config.yaml"), '--no-meta'])

    assert result.exit_code == 0
    expected_files = [
        'coordinates.csv',
        'footprint_performance.csv',
        'algorithm_bin.csv',
        'beta_easy.csv',
        'good_algos.csv'
    ]
    for file in expected_files:
        assert (rootdir_path / file).exists()
    assert "Running Instance Space Analysis" in result.stdout
    assert "Instance Hardness analysis finished." in result.stdout


def test_full_chain(rootdir_path, classification_dataset):
    classification_dataset.to_csv(rootdir_path / "data.csv", index=False)

    result = runner.invoke(cli, ['run', '--config', str(rootdir_path / "config.yaml")])

    assert result.exit_code == 0
    expected_files = [
        'metadata.csv',
        'coordinates.csv',
        'footprint_performance.csv',
        'algorithm_bin.csv',
        'beta_easy.csv',
        'good_algos.csv',
        'ih.csv'
    ]
    for file in expected_files:
        assert (rootdir_path / file).exists()
    assert "Instance Hardness analysis finished." in result.stdout

    result = runner.invoke(cli, ['run', '--config', str(rootdir_path / "config.yaml"), '--no-meta', '--no-isa'])
    assert result.exit_code == 0


def test_with_fs(rootdir_path, config_classification, classification_dataset):
    classification_dataset.to_csv(rootdir_path / "data.csv", index=False)

    conf = load_yaml_file(rootdir_path / "config.yaml")
    conf['fs']['enabled'] = True
    write_yaml_file(conf, rootdir_path / "config.yaml")

    result = runner.invoke(cli, ['run', '--config', str(rootdir_path / "config.yaml")])

    assert result.exit_code == 0
    assert "Feature selection on" in result.stdout
    assert "Instance Hardness analysis finished." in result.stdout


def test_with_hpo(rootdir_path, config_classification, classification_dataset):
    classification_dataset.iloc[:500, :].to_csv(rootdir_path / "data.csv", index=False)

    conf = load_yaml_file(rootdir_path / "config.yaml")
    conf['hpo']['enabled'] = True
    write_yaml_file(conf, rootdir_path / "config.yaml")

    result = runner.invoke(cli, ['run', '--config', str(rootdir_path / "config.yaml"), '--no-isa'])

    assert result.exit_code == 0
    assert "Hyper parameter optimization enabled" in result.stdout
    assert "Instance Hardness analysis finished." in result.stdout

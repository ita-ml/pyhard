import pytest
from sklearn import datasets

from pyhard.thirdparty.skfeature import ITFS

eps = 1e-3


@pytest.mark.parametrize("n_features, n_informative", [(20, 5), (20, 10), (20, 15), (40, 10), (40, 20)])
def test_toy_dataset(n_features, n_informative):
    X, y, coef = datasets.make_regression(
        n_samples=1000,
        n_features=n_features,
        n_informative=n_informative,
        bias=0,
        noise=1e-3,
        coef=True,
        shuffle=False,
        random_state=0
    )

    fs_reg = ITFS()
    fs_reg.fit(X, y)
    assert 0.8 * n_informative - 0.1 * n_features <= fs_reg.n_features_ <= 1.5 * n_informative + 0.2 * n_features


@pytest.mark.parametrize("n_features, n_informative", [(20, 5), (20, 10), (20, 15), (40, 10), (40, 20)])
def test_select_n(n_features, n_informative):
    X, y, coef = datasets.make_regression(
        n_samples=1000,
        n_features=n_features,
        n_informative=n_informative,
        bias=0,
        noise=1e-3,
        coef=True,
        shuffle=False,
        random_state=0
    )

    fs_reg = ITFS(n_features_to_select=n_informative)
    fs_reg.fit(X, y)
    assert fs_reg.n_features_ == n_informative


@pytest.mark.parametrize("criteria", ['mrmr', 'cmim', 'mim', 'mifs', 'cife', 'jmi', 'disr', 'icap'])
def test_methods(criteria):
    X, y, coef = datasets.make_regression(
        n_samples=1000,
        n_features=40,
        n_informative=10,
        bias=0,
        noise=1e-3,
        coef=True,
        shuffle=False,
        random_state=0
    )

    fs_reg = ITFS(n_features_to_select=10, criteria=criteria)
    fs_reg.fit(X, y)
    assert fs_reg.n_features_ == 10

    fs_reg.n_features_to_select = None
    fs_reg.fit(X, y)
    assert fs_reg.n_features_ < 40

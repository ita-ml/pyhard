import pandas as pd

from pyhard.integrator import build_metadata


def test_build_classification(config_classification, classification_dataset):
    df_meta, df_ih = build_metadata(classification_dataset, config_classification, return_ih=True)

    assert isinstance(df_meta, pd.DataFrame)
    assert isinstance(df_ih, pd.DataFrame)
    assert not df_meta.empty
    assert not df_ih.empty
    assert df_meta.shape[1] == len(config_classification.algos.pool) + len(config_classification.measures.list)
    assert df_meta.shape[0] == df_ih.shape[0] == classification_dataset.shape[0]


def test_build_regression(config_regression, regression_dataset):
    df_meta, df_ih = build_metadata(regression_dataset, config_regression, return_ih=True)

    assert isinstance(df_meta, pd.DataFrame)
    assert isinstance(df_ih, pd.DataFrame)
    assert not df_meta.empty
    assert not df_ih.empty
    assert df_meta.shape[1] == len(config_regression.algos.pool) + len(config_regression.measures.list)
    assert df_meta.shape[0] == df_ih.shape[0] == regression_dataset.shape[0]

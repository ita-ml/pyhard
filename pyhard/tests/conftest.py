import os
import shutil
import tempfile
from dataclasses import dataclass
from pathlib import Path
from typing import Literal

import numpy as np
import pandas as pd
import pytest
from pyispace.example import save_opts
from sklearn import datasets

from pyhard.structures import (
    GeneralConfig,
    MeasuresConfig,
    AlgorithmsConfig,
    FSConfig,
    HPOConfig,
    ISAConfig,
    Configurations
)
from pyhard.utils import write_yaml_file


@dataclass
class Args:
    generate: bool = False
    app: bool = False
    demo: bool = False
    meta: bool = True
    isa: bool = True
    verbose: bool = False
    browser: bool = False
    other: str = None


def make_sample_config(problem: Literal["classification", "regression"]) -> Configurations:
    general = GeneralConfig(rootdir='.', datafile='data.csv', problem=problem)
    if problem == "classification":
        measures = MeasuresConfig(
            list=['kDN', 'DCP', 'TD_P', 'TD_U', 'CL', 'CLD', 'N1', 'N2', 'LSC', 'LSR', 'F1', 'F2', 'F3']
        )
        algos = AlgorithmsConfig(
            pool=['bagging', 'svc_linear', 'svc_rbf', 'logistic_regression'],
            metric='logloss'
        )
    elif problem == "regression":
        measures = MeasuresConfig(
            list=['C4', 'L1', 'S1', 'S2', 'S3', 'FO', 'POT', 'HB', 'EDS', 'EZR']
        )
        algos = AlgorithmsConfig(
            pool=['ada_boost', 'svr_linear', 'svr_epsilon', 'decision_tree'],
            metric='absolute_error'
        )
    else:
        raise ValueError
    fs = FSConfig(enabled=False)
    hpo = HPOConfig(enabled=False)
    isa = ISAConfig()
    return Configurations(general=general, measures=measures, algos=algos, fs=fs, hpo=hpo, isa=isa)


@pytest.fixture
def cli_args():
    return Args()


@pytest.fixture
def config_classification():
    return make_sample_config("classification")


@pytest.fixture
def config_regression():
    return make_sample_config("regression")


@pytest.fixture(scope="function")
def rootdir_path(tmp_path_factory):
    """
    Fixture that creates a temporary root dir from which PyHard's 'main' reads and writes data. It also populate it with
    configuration files ('config.yaml' and 'options.json').
    """
    tmp_dir = tmp_path_factory.mktemp("test")

    conf = make_sample_config("classification")
    conf.general.rootdir = str(tmp_dir)
    conf.general.datafile = str(tmp_dir / "data.csv")

    # saves config.yaml
    write_yaml_file(conf.to_dict(), tmp_dir / "config.yaml")

    # saves options.json
    save_opts(tmp_dir)

    return tmp_dir


@pytest.fixture(scope="module")
def my_filepath():
    tmpdir = tempfile.mkdtemp()
    subdir = os.path.join(tmpdir, "test")
    os.mkdir(subdir)
    yield Path(subdir)
    shutil.rmtree(tmpdir)


@pytest.fixture(scope="function")
def classification_dataset() -> pd.DataFrame:
    X, y = datasets.make_classification(
        n_samples=300,
        n_features=5,
        n_informative=5,
        n_redundant=0,
        n_classes=3,
        n_clusters_per_class=1,
        random_state=1
    )
    data = pd.DataFrame(X)
    data = data.add_prefix('f_')
    data['target'] = y
    return data


@pytest.fixture(scope="function")
def regression_dataset() -> pd.DataFrame:
    X, y = datasets.make_regression(
        n_samples=1000,
        n_features=15,
        n_informative=10,
        noise=1e-3,
        random_state=0
    )
    data = pd.DataFrame(X)
    data = data.add_prefix('f_')
    data['target'] = y
    return data


@pytest.fixture(scope="session")
def boston():
    boston = datasets.load_boston()
    data = pd.DataFrame(
        data=np.c_[boston.data, boston.target],
        columns=boston.feature_names.tolist() + ['target']
    )
    return data


@pytest.fixture(scope="session")
def breast_cancer():
    X, y = datasets.load_breast_cancer(as_frame=True, return_X_y=True)
    data = pd.concat([X, y], axis=1)
    data.dropna(inplace=True)
    return data


@pytest.fixture(scope="session")
def mix() -> pd.DataFrame:
    X, y = datasets.make_blobs(
        n_samples=1000,
        n_features=2,
        centers=[[0, 0], [0, 0]],
        cluster_std=2,
        random_state=0
    )

    return pd.DataFrame(np.hstack([X, y.reshape(-1, 1)]), columns=['x1', 'x2', 'y'])


@pytest.fixture(scope="session")
def overlap() -> pd.DataFrame:
    X, y = datasets.make_blobs(
        n_samples=1000,
        n_features=2,
        centers=[[0, 0], [4, 4]],
        cluster_std=2,
        random_state=0
    )

    return pd.DataFrame(np.hstack([X, y.reshape(-1, 1)]), columns=['x1', 'x2', 'y'])


@pytest.fixture(scope="session")
def separate() -> pd.DataFrame:
    X, y = datasets.make_blobs(
        n_samples=1000,
        n_features=2,
        centers=[[0, 0], [10, 10]],
        cluster_std=2,
        random_state=0
    )

    return pd.DataFrame(np.hstack([X, y.reshape(-1, 1)]), columns=['x1', 'x2', 'y'])
